/*
 * Copyright 2018 Daniel Gultsch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.quicksy.server.controller;

import com.google.common.collect.*;
import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;

import im.quicksy.server.configuration.Configuration;
import im.quicksy.server.database.Database;
import im.quicksy.server.throttle.Strategy;
import im.quicksy.server.throttle.VolumeLimiter;
import im.quicksy.server.xmpp.synchronization.Entry;
import im.quicksy.server.xmpp.synchronization.PhoneBook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rocks.xmpp.addr.Jid;
import rocks.xmpp.core.session.XmppSession;
import rocks.xmpp.core.stanza.AbstractIQHandler;
import rocks.xmpp.core.stanza.model.IQ;
import rocks.xmpp.core.stanza.model.StanzaError;
import rocks.xmpp.core.stanza.model.errors.Condition;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class SynchronizationController extends AbstractIQHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(SynchronizationController.class);

    private static final VolumeLimiter<Jid, String> PHONE_NUMBER_LIMITER =
            new VolumeLimiter<>(Strategy.of(Duration.ofDays(1), 2048));

    private final XmppSession xmppSession;
    private final List<Jid> peers;

    public SynchronizationController(final XmppSession xmppSession) {
        super(PhoneBook.class, IQ.Type.GET);
        this.xmppSession = xmppSession;
        this.peers = Configuration.getInstance().getPeers();
    }

    @Override
    protected IQ processRequest(IQ iq) {
        final PhoneBook phoneBook = iq.getExtension(PhoneBook.class);
        final Jid requester = iq.getFrom().asBareJid();
        if (phoneBook == null) {
            return iq.createError(Condition.BAD_REQUEST);
        }
        final var domain = Configuration.getInstance().getDomain();
        final var requesterIsPeer = this.peers.contains(requester);
        final var phoneNumbers = phoneBook.getPhoneNumbers();
        if (requesterIsPeer) {
            LOGGER.info("Request came from authorized peer {}", requester);
        } else if (requester.getDomain().equals(domain)) {
            try {
                PHONE_NUMBER_LIMITER.attempt(requester, phoneNumbers);
            } catch (VolumeLimiter.RetryInException e) {
                return iq.createError(new StanzaError(Condition.POLICY_VIOLATION, e.getMessage()));
            }
        } else {
            return iq.createError(Condition.NOT_AUTHORIZED);
        }
        LOGGER.info("{} requested to sync {} phone numbers", requester, phoneNumbers.size());
        final ListMultimap<String, Jid> numberToJabberIds = ArrayListMultimap.create();
        final List<String> existingUsersOnQuicksy =
                Database.getInstance().findExistingUsers(domain, phoneNumbers);
        for (final String phoneNumber : existingUsersOnQuicksy) {
            numberToJabberIds.put(phoneNumber, Jid.of(phoneNumber, domain, null));
        }
        final List<Database.RawEntry> directoryUsers =
                Database.getInstance().findDirectoryUsers(phoneNumbers);
        for (Database.RawEntry rawEntry : directoryUsers) {
            numberToJabberIds.put(rawEntry.getPhoneNumber(), rawEntry.getJid());
        }
        final List<Entry> entriesOnPeers;
        if (requesterIsPeer) {
            entriesOnPeers = Collections.emptyList();
        } else {
            entriesOnPeers = findOnPeers(this.peers, phoneNumbers);
        }
        for (final Entry peerEntry : entriesOnPeers) {
            for (final Jid jid : peerEntry.getJids()) {
                numberToJabberIds.put(peerEntry.getNumber(), jid);
            }
        }
        final Collection<Entry> entries =
                Collections2.transform(
                        numberToJabberIds.asMap().entrySet(),
                        entry -> new Entry(entry.getKey(), ImmutableSet.copyOf(entry.getValue())));
        final String hash = hashedEntries(entries);
        if (hash.equals(phoneBook.getVer())) {
            LOGGER.info("hash hasn't changed for {} ({} entries)", requester, entries.size());
            return iq.createResult();
        }
        LOGGER.info("responding to {} with {} entries", requester, entries.size());
        return iq.createResult(new PhoneBook(entries));
    }

    private static String hashedEntries(final Collection<Entry> entries) {
        final StringBuilder builder = new StringBuilder();
        for (final Entry entry : Ordering.natural().sortedCopy(entries)) {
            if (builder.length() != 0) {
                builder.append('\u001d');
            }
            builder.append(entry.getNumber());
            for (final Jid jid : Ordering.natural().sortedCopy(entry.getJids())) {
                builder.append('\u001e');
                builder.append(jid.asBareJid().toEscapedString());
            }
        }
        return BaseEncoding.base64()
                .encode(
                        Hashing.sha1()
                                .hashString(builder.toString(), StandardCharsets.UTF_8)
                                .asBytes());
    }

    private CompletableFuture<Collection<Entry>> findOnPeer(
            final Jid peer, final Collection<String> numbers) {
        final var entries =
                Collections2.transform(numbers, n -> new Entry(n, Collections.emptyList()));
        final IQ iq = new IQ(peer, IQ.Type.GET, new PhoneBook(entries));
        return xmppSession
                .query(iq)
                .thenApply(
                        response -> {
                            final var phoneBook = response.getExtension(PhoneBook.class);
                            final Collection<Entry> results;
                            if (phoneBook == null) {
                                results = Collections.emptyList();
                            } else {
                                results = phoneBook.getValidEntries();
                            }
                            if (results.size() > 0) {
                                LOGGER.info("Found {} entries on peer {}", results.size(), peer);
                            }
                            return results;
                        })
                .toCompletableFuture()
                .orTimeout(5, TimeUnit.SECONDS)
                .exceptionally(
                        ex -> {
                            LOGGER.warn("Could not retrieve entries from peer {}", peer, ex);
                            return Collections.emptyList();
                        });
    }

    private List<Entry> findOnPeers(final Collection<Jid> peers, final Collection<String> numbers) {
        return peers.stream()
                .map(peer -> findOnPeer(peer, numbers))
                .map(CompletableFuture::join)
                .flatMap(Collection::stream)
                .toList();
    }
}
