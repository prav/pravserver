/*
 * Copyright 2018 Daniel Gultsch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.quicksy.server.verification;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import im.quicksy.server.configuration.Configuration;
import im.quicksy.server.verification.twilio.ErrorResponse;
import im.quicksy.server.verification.twilio.GenericResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;

public class TwilioVerificationProvider extends AbstractVerificationProvider {

    public static final int PHONE_VERIFICATION_INCORRECT = 60022;
    public static final int PHONE_VERIFICATION_NOT_FOUND = 60023;
    public static final int PHONE_NUMBER_IS_INVALID = 60033;
    public static final int PHONE_NUMBER_IS_NOT_A_VALID_MOBILE_NUMBER = 21614;

    private static final String TWILIO_API_URL = "https://verify.twilio.com/v2/Services/";
    private static final Logger LOGGER = LoggerFactory.getLogger(TwilioVerificationProvider.class);
    private final GsonBuilder gsonBuilder = new GsonBuilder();
    private final PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

    private final String accountSid;
    private final String serviceSid;
    private final String authToken;

    private static final HttpClient client = HttpClient.newHttpClient();

    public TwilioVerificationProvider(Map<String, String> parameter) {
        super(parameter);
        this.accountSid = Preconditions.checkNotNull(parameter.get("account_sid"));
        this.serviceSid = Preconditions.checkNotNull(parameter.get("service_sid"));
        this.authToken = Preconditions.checkNotNull(parameter.get("auth_token"));
    }

    public TwilioVerificationProvider() {
        super(Collections.emptyMap());
        final TreeMap<String, Configuration.ProviderConfiguration> provider =
                Configuration.getInstance().getProvider();
        final Configuration.ProviderConfiguration myConfiguration =
                provider.get(getClass().getName());
        if (myConfiguration == null) {
            throw new RuntimeException("No configuration found for " + getClass().getSimpleName());
        }
        this.accountSid =
                Preconditions.checkNotNull(myConfiguration.getParameter().get("account_sid"));
        this.serviceSid =
                Preconditions.checkNotNull(myConfiguration.getParameter().get("service_sid"));
        this.authToken =
                Preconditions.checkNotNull(myConfiguration.getParameter().get("auth_token"));
    }

    @Override
    public boolean verify(Phonenumber.PhoneNumber phoneNumber, String pin)
            throws RequestFailedException {
        Map<String, String> params = new HashMap<>();
        params.put(
                "To", phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164));
        params.put("Code", pin);
        try {
            final String method = "VerificationCheck";
            final GenericResponse response = execute(method, params, GenericResponse.class);
            LOGGER.info("twilio message was " + response.getStatus());
            if (response.isSuccess()) {
                LOGGER.info("The pin entered for {} is valid", phoneNumber);
            } else {
                LOGGER.warn("The pin entered for {} is invalid", phoneNumber);
            }
            return response.isSuccess();
        } catch (RequestFailedException e) {
            throw e;
        }
    }

    @Override
    public void request(Phonenumber.PhoneNumber phoneNumber, Method method)
            throws RequestFailedException {
        request(phoneNumber, method, null);
    }

    @Override
    public void request(Phonenumber.PhoneNumber phoneNumber, Method method, String language)
            throws RequestFailedException {
        LOGGER.info("requesting verification (" + method.toString() + ") for " + phoneNumber);
        Map<String, String> params = new HashMap<>();
        params.put("Channel", method.toString().toLowerCase(Locale.ENGLISH));
        params.put(
                "To", phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164));
        try {
            final GenericResponse response =
                    execute("Verifications", params, GenericResponse.class);
            if (!response.wentThrough()) {
                throw new RequestFailedException(response.getStatus());
            }
        } catch (RequestFailedException e) {
            throw e;
        }
    }

    private String getQuery(Map<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();

        for (Map.Entry<String, String> param : params.entrySet()) {

            if (result.length() != 0) {
                result.append('&');
            }

            result.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(param.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    private <T> T execute(final String method, final Map<String, String> params, Class<T> clazz)
            throws RequestFailedException {
        String result = null;
        try {
            final Gson gson = this.gsonBuilder.create();
            String credentials = this.accountSid + ":" + this.authToken;
            String auth = "Basic " + Base64.getEncoder().encodeToString(credentials.getBytes());

            HttpRequest request =
                    HttpRequest.newBuilder()
                            .uri(URI.create(TWILIO_API_URL + this.serviceSid + "/" + method))
                            .headers("Content-Type", "application/x-www-form-urlencoded")
                            .header("Authorization", auth)
                            .POST(HttpRequest.BodyPublishers.ofString(getQuery(params)))
                            .build();
            LOGGER.info(getQuery(params));
            HttpResponse<String> response =
                    client.send(request, HttpResponse.BodyHandlers.ofString());

            int code = response.statusCode();
            LOGGER.warn("code was " + Integer.toString(code));
            result = response.body();

            if (code >= 200 && code < 300) {
                return gson.fromJson(result, clazz);
            } else {
                LOGGER.warn("json was " + result);
                final ErrorResponse error = gson.fromJson(result, ErrorResponse.class);
                if (error.getErrorCode() == PHONE_VERIFICATION_NOT_FOUND) {
                    throw new TokenExpiredException(error.getStatus(), error.getErrorCode());
                } else {
                    throw new RequestFailedException(error.getStatus(), error.getErrorCode());
                }
            }
        } catch (JsonSyntaxException e) {
            final String firstLine = result == null ? "" : result.split("\n")[0];
            throw new RequestFailedException(
                    "Unable to parse JSON starting with "
                            + firstLine.substring(0, Math.min(firstLine.length(), 20)),
                    e);
        } catch (RequestFailedException e) {
            throw e;
        } catch (Throwable t) {
            throw new RequestFailedException(t);
        }
    }
}
